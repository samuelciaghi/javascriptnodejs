const express = require('express');
const cors = require('cors');
const porta = process.env.PORT || 3000;
const applicazione = express();
let persone = require('./persone.json');
require('dotenv').config();

applicazione.use(express.urlencoded({ extended: false }));
applicazione.use(express.json());
applicazione.use(cors({ credentials: true, origin: true }));
applicazione.use( express.static(__dirname + '/Front-end'));

applicazione.get('/', (req, res) => {

    let html;

    html = '<a href="http://localhost:3000/persone"><button>CARICA PERSONE</button></a>';

    res.send(html);

});

applicazione.get('/persone', (req, res) => {
    res.send(persone)
});

applicazione.get('/tab',(req, res) => {
    res.sendFile( __dirname + "/Front-End/")
});

applicazione.listen(porta, () => console.log('Applicazione in ascolto sulla porta :' + porta));
