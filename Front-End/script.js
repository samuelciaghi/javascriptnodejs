let tabella = document.getElementById('tabellaPersone');
let datiRicevuti = [];
let inputCerca = document.getElementById('inputCerca');

async function fetchDati() {
    return (await fetch('http://localhost:3000/persone')).json()
}
fetchDati();

async function riceviDati() {

    datiRicevuti = await fetchDati();
    creaTabella(datiRicevuti)

    console.log(datiRicevuti);

}
riceviDati();

function creaTabella(datiTab) {

    let datiTabella;
    /*  datiTabella = datiTab.map(persona => {
 
         return (`<tr>
              <td> ${persona.id} </td> 
             <td>  ${persona.nome}  </td> 
             <td>  ${persona.cognome}  </td> 
             <td>  ${persona.email} </td> 
             <td class="nascondi" >  ${persona.gender}  </td>
              </tr>`)
 
     }).join(""); */

    datiTabella = datiTab.map(persona => {

        return ('<tr>' +
            '<td>' + persona.id + '</td>' +
            '<td>' + persona.nome + '</td>' +
            '<td>' + persona.cognome + '</td>' +
            '<td>' + persona.email + '</td>' +
            '<td class="nascondi" >' + persona.lavoro + '</td>' +
            '</tr>')

    }).join("");

    tabella.innerHTML += datiTabella;
}

function filtroRicerca() {

    let valoreInput, tr, td, i;
    valoreInput = inputCerca.value.toUpperCase();
    tr = tabella.getElementsByTagName("tr");

    for (i = 0; i < tr.length; i++) {

        td = tr[i].getElementsByTagName("td")[1];   // Ricerca in base al nome

        if (td) {
            if (td.innerHTML.toUpperCase().indexOf(valoreInput) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}


